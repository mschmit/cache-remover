@ECHO OFF

setLocal
    call :setESC
    cls

    SET UBOATPATH="%AppData%\..\LocalLow\Deep Water Studio\UBOAT\"

    DIR uboat_header.txt >NUL 2>&1 && TYPE uboat_header.txt

    ECHO This script will clean the cache of %ESC%[1mUBOAT%ESC%[0m by removing three folders:
    ECHO.
    ECHO  - Temp
    ECHO  - Cache
    ECHO  - Data Sheets
    ECHO.
    Echo  In %UBOATPATH%

    ECHO.

    :PROMPT
	SET DELETEALL="N"
    SET ERRORINFO=0
	SET /P DELETEALL="Do you want to process (Y/N)?"

    IF /i "%DELETEALL%" == "Y" ( GOTO clearCACHE ) ELSE ( GOTO END )


    :clearCACHE
        ECHO.
        CD "%AppData%\..\LocalLow\Deep Water Studio\UBOAT\" && ECHO  -- Moving to UBOAT folder... %ESC%[92mDone%ESC%[0m! || ECHO. && ECHO %ESC%[31mERROR: UBOAT folder not found.%ESC%[0m && GOTO END
        ECHO.
        CALL :removeFOLDER "Cache"
        CALL :removeFOLDER "Temp"
        CALL :removeFOLDER "Data Sheets"

        IF %ERRORINFO% == 1 (
            ECHO INFO: "folder %ESC%[33malready removed%ESC%[0m!" isn't a bad thing since removing folders is precisely what we want to achieve. && ECHO.
        )
        ECHO %ESC%[1mUBOAT%ESC%[0m cache has been cleared!
        GOTO END

    :removeFOLDER
        IF "%~1" == "" (  ECHO %ESC%[31mINTERNAL ERROR: removeFOLDER has been called without argument.%ESC%[0m && GOTO END )
        IF EXIST "%~1" (
            RMDIR /s /q  "%~1" && ECHO  -- removing  %ESC%[1m"%~1"%ESC%[0m folder...  %ESC%[92mDone%ESC%[0m!
        ) ELSE (
            ECHO  -- %ESC%[1m"%~1"%ESC%[0m folder %ESC%[33malready removed%ESC%[0m!
            SET ERRORINFO=1
        )
        ECHO.
        EXIT /B 0

    :setESC
        for /F %%a in ('"prompt $E$S & echo on & for %%b in (1) do rem"') do set "ESC=%%a"
        exit /B

    :END
      ECHO.

endlocal

PAUSE
